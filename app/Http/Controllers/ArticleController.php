<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Article;

class ArticleController extends Controller
{
    public function create(){
        return view('articles.create');
    }

    public function store(request $request){
        //dd($request->all());
        $request->validate([
            'judul' => 'required',
            'isi_artikel' => 'required'
        ]);

        $post = Article::create([
            "judul" => $request["judul"],
            "isi_artikel" => $request["isi_artikel"] 
        ]);

        return redirect('/articles')->with('success','Artikel Berhasil Dibuat!');
    }

    public function index(){
         
        $article = Article::all();
        return view('articles.index', compact('article'));
    }

    public function show($id){
        $post = Article::find($id);
        return view('articles.show', compact('post'));
    }

    public function edit($id){
        $post = Article::find($id);
        return view('articles.edit', compact('post'));
    }

    public function update($id, request $request){

        $update = Article::where('id',$id)->update([
            "judul" => $request["judul"],
            "isi_artikel" => $request["isi_artikel"] 
        ]);
        
        return redirect('/articles')->with('success','Berhasil update artikel!');
    }

    public function destroy($id){
        Article::destroy($id);
        return redirect('/articles')->with('success','Artikel berhasil dihapus');
    }
}
