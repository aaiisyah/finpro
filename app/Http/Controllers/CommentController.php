<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Comment;
use App\User;

class CommentController extends Controller
{
    public function index(){
        $post = Comment::all();
        return view('comment', compact('post'));
    }

    public function store(request $request){
        //dd($request->all());
        $request->validate([
            'isi_komentar' => 'required',
            
        ]);

        $post = Comment::create([
            "isi_komentar" => $request["isi_komentar"]
            
        ]);

        return redirect('/articles')->with('success','Data Berhasil Dibuat!');
    }

    public function show($id){
        $post = Comment::find($id);
        return view('comment', compact('post'));
    }

    public function edit($id){
        $post = Comment::find($id);
        return view('comment', compact('post'));
    }

    public function update($id, request $request){

        $update = Comment::where('id',$id)->update([
            "isi_komentar" => $request["isi_komentar"]
            
        ]);
        
        return redirect('comment')->with('success','Berhasil update data!');
    }
}
