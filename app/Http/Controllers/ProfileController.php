<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Profile;

class ProfileController extends Controller
{

    public function index(){
        $post = Profile::all();
        return view('profile', compact('post'));
    }

    public function store(request $request){
        //dd($request->all());
        $request->validate([
            'nama_lengkap' => 'required',
            // 'photo' => '',
            'jumlah_artikel' => 'required',
            'jumlah_following' => 'required',
            'jumlah_follower' => 'required'
        ]);

        $post = Profile::create([
            "nama_lengkap" => $request["nama_lengkap"],
            // "photo" => $request["photo"],
            "jumlah_artikel" => $request["jumlah_artikel"],
            "jumlah_following" => $request["jumlah_following"],
            "jumlah_follower" => $request["jumlah_follower"],
        ]);

        return redirect('profile')->with('success','Data Berhasil Dibuat!');
    }

    public function show($id){
        $post = Profile::find($id);
        return view('profile', compact('post'));
    }

    public function edit($id){
        $post = Profile::find($id);
        return view('profile', compact('post'));
    }

    public function update($id, request $request){

        $update = Profile::where('id',$id)->update([
            "nama_lengkap" => $request["nama_lengkap"],
            // "photo" => $request["photo"],
            "jumlah_artikel" => $request["jumlah_artikel"],
            "jumlah_following" => $request["jumlah_following"],
            "jumlah_follower" => $request["jumlah_follower"], 
        ]);
        
        return redirect('profile')->with('success','Berhasil update data!');
    }

}
