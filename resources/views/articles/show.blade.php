@extends('..layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="card mb-4">
        <div class="card-body">
        <div class="form-group">
        <h2 class="card-title">{{$post->judul}}</h2>
        <p class="card-text">{{$post->isi_artikel}}</p>

        <form role="form" action="{{ route('comments.store',['comment'=>$post->id]) }}" method="post">
        @csrf
        <h5 class="card-title">Post Comment:</h5>
        <textarea class="form-control" id="isi_komentar" name="isi_komentar" value="{{ old('isi_komentar','') }}"  style="height:200px" placeholder="Isi Komentar"></textarea>
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>

        <br><p class="card-text">Komentar:</p> -->
        
        <p class="card-text">{{$post->isi_komentar}}</p>
        

        </div>
        </div>  
        </div>
        </div>
        </div>
    </div>
@endsection