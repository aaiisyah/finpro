@extends('..layouts.app')
@section('content')
<div class="container">
<div class="row justify-content-center">
<div class="col-md-8">
<h1 class="my-4">Buat Arikel Baru</h1>
<form role="form" action="/articles" method="post">
    @csrf
    @method('POST')
    <div class="card-body">
        <div class="form-group">
            <label for="judul">Judul Artikel</label>
            <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul','') }}" placeholder="Judul Artikel">
            @error('judul')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <br><br>
            <label for="isi_artikel">Isi Artikel</label>
            <textarea class="form-control" id="isi_artikel" name="isi_artikel" value="{{ old('isi_artikel','') }}"  style="height:500px" placeholder="Isi Artikel"></textarea>
            @error('isi_artikel')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
</form>
</div>
</div>
</div>
@endsection