@extends('..layouts.app')
@section('content')
<div class="container">
<div class="row justify-content-center">
<div class="col-md-8">
<h1 class="my-4">Edit Artikel</h1>
<form role="form" action="/articles/{{$post->id}}" method="post">
    @csrf
    @method('PUT')
    <div class="card mb-4">
    <div class="card-body">
        <div class="form-group">
            <label for="judul">Judul Artikel</label>
            <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul',$post->judul) }}" placeholder="Judul Artikel">
            @error('judul')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <br><br>
            <label for="isi_artikel">Isi Artikel</label>
            <input type="text" class="form-control" id="isi_artikel" name="isi_artikel" value="{{ old('isi_artikel',$post->isi_artikel) }}" placeholder="Isi Artikel">
            @error('isi_artikel')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
        </form>
</div>
</div>
</div>
@endsection