

@extends('layouts.app')
@section('content')

<div class="container">
<div class="row justify-content-center">
<div class="col-md-8">
<div class="card">

    @csrf
    @method('POST')
    <div class="card mb-4">
    <div class="card-body">
        <div class="form-group">
            <label for="nama_lengkap">Nama Lengkap</label>
            <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" value="{{ old('nama_lengkap',$post->nama_lengkap) }}" placeholder="Nama Lengkap">

            <br><br>
            <label for="photo">Photo</label>
            <input type="text" class="form-control" id="photo" name="photo" value="{{ old('photo',$post->photo) }}" placeholder="Photo">
           
            <br><br>
            <label for="jumlah_artikel">Jumlah Artikel</label>
            <input type="text" class="form-control" id="jumlah_artikel" name="jumlah_artikel" value="{{ old('jumlah_artikel',$post->jumlah_artikel) }}" placeholder="Jumlah Artikel">
           
            <br><br>
            <label for="jumlah_following">Jumlah Following</label>
            <input type="text" class="form-control" id="jumlah_following" name="jumlah_following" value="{{ old('jumlah_following',$post->jumlah_following) }}" placeholder="Jumlah Following">
        
            <br><br>
            <label for="jumlah_follower">Jumlah Follower</label>
            <input type="text" class="form-control" id="jumlah_follower" name="jumlah_follower" value="{{ old('jumlah_follower',$post->jumlah_follower) }}" placeholder="Jumlah Follower">

        </div>
        <!-- /.card-body -->

        <div class="card-footer">
        <!-- <form role="form" action="/articles/create" method="post"> -->
            <a class="btn btn-primary" href="/articles/create">Buat Artikel Baru</a>
        <!-- </form> -->
        </div>   
            </div>
            </div>
            </div>
            </div>
        </div>

@endsection