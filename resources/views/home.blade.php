
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
            <div class="card">
            @forelse($article as $key => $post)
                <!-- <img src="http://placehold.it/750x300"> -->
                <div class="card-body">
                
                <h2 class="card-title">{{$post->judul}}</h2>
                <p class="card-text">{{substr($post->isi_artikel, 0, 250)}}</p>
                <a href="{{ route('articles.show',['article'=>$post->id]) }}" class="btn btn-primary">Read More &rarr;</a>
                
                </div>
                <div class="card-footer text-muted">
                Posted on {{$post->updated_at}} by
                <a href="#">{{ Auth::user()->name }}</a>
                <div class="d-flex">
                <!-- <a href="{{ route('articles.show',['article'=>$post->id]) }}" class="btn btn-info btn-sm">show</a> -->
                <a href="{{ route('articles.edit',['article'=>$post->id]) }}" class="btn btn-default btn-sm">edit</a>
                <form action="{{ route('articles.destroy',['article'=>$post->id]) }}" method="post">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
                </div>
                </div>
                @empty
                Tidak Ada Artikel
                @endforelse    
            </div>
            
            

            <!-- Pagination -->
            <ul class="pagination justify-content-center">
                <li class="page-item">
                <a class="page-link" href="#">&larr; Older</a>
                </li>
                <li class="page-item disabled">
                <a class="page-link" href="#">Newer &rarr;</a>
                </li>
            </ul>

            </div>
            
        </div>
        </div>
    </div>
</div>
@endsection
